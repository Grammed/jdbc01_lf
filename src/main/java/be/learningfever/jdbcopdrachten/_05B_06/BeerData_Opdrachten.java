package be.learningfever.jdbcopdrachten._05B_06;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BeerData_Opdrachten {
    public JdbcFacade jdbcFacade;

    public BeerData_Opdrachten() {
        jdbcFacade = new JdbcFacade();
    }

    public BeerData_Opdrachten(JdbcFacade jdbcFacade1) {
        this.jdbcFacade = jdbcFacade1;
    }

    public String getBeerById(int id) throws SQLException {
        String sql = ("select * from Beers where Id = ?;");
        PreparedStatement preparedStatement = jdbcFacade.prepareStatement(sql);
        preparedStatement.setInt(1, id);
        ResultSet results = preparedStatement.executeQuery();

        if (results.next()) {
            int ident = results.getInt("Id");
            String name = results.getString("Name");
            int brewer = results.getInt("BrewerId");
            int category = results.getInt("CategoryId");
            float price = results.getFloat("Price");
            int stock = results.getInt("Stock");
            float alcohol = results.getFloat("Alcohol");
            int version = results.getInt("Version");
            Blob image = results.getBlob("Image");
            return String.format("Id %d, Name %s, BrewerId %d, CategoryId %d, Price %.2f, Stock %d," +
                            " Alcohol %.2f, Version %d, Image %s", ident, name, brewer, category, price, stock,
                    alcohol, version, image);
        }

        return "null";

    }

    // Helper Method for getBeerByIdAndChangePrice
    public float getBeerPriceById(int id) throws SQLException {
        String sql = ("select * from Beers where Id = ?;");
        PreparedStatement preparedState = jdbcFacade.prepareStatement(sql);

        preparedState.setInt(1, id);
        ResultSet results = preparedState.executeQuery();

        if (results.next()) {
            return results.getFloat("Price");
        }

        return 0.00f;
    }

    public int getBeerByIdAndChangePrice(int id, double price) throws SQLException {
        String sql = ("update Beers set `Price` = ? where Id = ? ;");
        PreparedStatement preparedStatement = jdbcFacade.prepareStatement(sql);

        preparedStatement.setDouble(1, price);
        preparedStatement.setInt(2, id);

        return preparedStatement.executeUpdate();
    }

    public ResultSet printBeersByAlcoholVolume(float alc) throws SQLException {
        String sql = "select * from Beers where Alcohol = ?;";

        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);

        prepared.setFloat(1, alc);
        return prepared.executeQuery();
    }

    public Double getAveragePriceByAlcoholVolume(float alc) throws SQLException {
        List<Float> fList = new ArrayList<>();
        String sql = "select * from Beers where Alcohol = ? ;";

        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);
        prepared.setFloat(1, alc);
        ResultSet result = prepared.executeQuery();

        while (result.next()) {
            fList.add(result.getFloat("Price"));
        }
        return fList.stream().mapToDouble(val -> val).average().orElse(0.0);
    }


    public int getBeersByAlcoholAndChangePrice(double alcohol, double price) throws SQLException {
        String sql = "update Beers set `Price` = ? where `Alcohol` = ?;";
        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);

        prepared.setDouble(1, price);
        prepared.setDouble(2, alcohol);
        return prepared.executeUpdate();
    }

    public int getBeersStockBySubName(String subName) throws SQLException {
        String sql = "Select SUM(`Stock`) from Beers where `Name` like ?;";

        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);
        prepared.setString(1, subName);
        ResultSet sum = prepared.executeQuery();

        if (sum.next()) {
            return sum.getInt("SUM(`Stock`)");

        }
        return 0;
    }

    public int getBeersBySubNameAndIncrementStock(String subName, int increment) throws SQLException {
        String sql = "update Beers set `Stock` = `Stock` + ? where `Name` like ? ;";

        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);
        prepared.setInt(1, increment);
        prepared.setString(2, subName);

        return prepared.executeUpdate();
    }

    public ResultSetMetaData getResultSetData(ResultSet results) throws SQLException {
        // get this method to print this data.
        return results.getMetaData();
    }

    public DatabaseMetaData getConnectionData(Connection connect) throws SQLException {
        // get this method to print this data.
        return connect.getMetaData();
    }

  /*  public int addATestBeer() throws SQLException {
        //Could not get this to work with a prepared statement. So reverted to this for now.
        String sql = "insert into Beers (`Name`, `BrewerId`, `CategoryId`, `Price`, `Stock`," +
                " `Alcohol`) values ('TestBeer', 12, 15, 2.22, 10, 3.8);";
        return jdbcFacade.executeUpdate(sql);
    }*/

    public void addATestBeer() throws SQLException {
        String name = "TestBeer";
        int brewer = 12;
        int category = 15;
        float price = 2.22f;
        int stock = 10;
        float alcohol = 3.8f;

        String sql = "insert into Beers (`Name`, `BrewerId`, `CategoryId`, `Price`, `Stock`," +
                " `Alcohol`) values (?, ?, ?, ?, ?, ?);";

        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);
        prepared.setString(1, name);
        prepared.setInt(2, brewer);
        prepared.setInt(3, category);
        prepared.setFloat(4, price);
        prepared.setInt(5, stock);
        prepared.setFloat(6, alcohol);
        prepared.executeQuery();
    }


    public String copyBeer(int id) throws SQLException {
        String name = "";
        int brewer = 0;
        int category = 0;
        float price = 0.0f;
        int stock = 0;
        float alcohol = 3.3f;
        int version = 4;
        Blob image = null;
        String beerToCopy = "select * from Beers where Id = ? ;";

        PreparedStatement prepared = jdbcFacade.prepareStatement(beerToCopy);
        prepared.setInt(1, id);
        ResultSet result = prepared.executeQuery();

        if (result.next()) {
            name = "Copy of " + (result.getString("Name"));
            brewer = result.getInt("BrewerId");
            category = result.getInt("CategoryId");
            price = result.getFloat("Price");
            stock = result.getInt("Stock");
            alcohol = result.getFloat("Alcohol");
            version = result.getInt("Version");
            image = result.getBlob("Image");
        }
        // The below allows for an updateable prepared statement, will now create prepareUpdateableStatement()
        Connection connect = jdbcFacade.getConnection();
        PreparedStatement prepFromConnection = connect.prepareStatement
                ("select * from Beers", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet results = prepFromConnection.executeQuery();


        results.moveToInsertRow();
        results.updateString("Name", name);
        results.updateInt("BrewerId", brewer);
        results.updateInt("CategoryId", category);
        results.updateFloat("Price", price);
        results.updateInt("Stock", stock);
        results.updateFloat("Alcohol", alcohol);
        results.updateInt("Version", version);
        results.updateBlob("Image", image);
        results.insertRow();
        //results.moveToCurrentRow(); // including this gives error you are behind first row..

        return results.getString("Name");

    }

    public int getCurrentMaxId() throws SQLException {

        ResultSet result = jdbcFacade.prepareStatement("select max(Id) from Beers;").executeQuery();

        if (result.next())
            return result.getInt("MAX(Id)");
        return 0;
    }

    public String getBeerNameById(int id) throws SQLException {
        String sql = "select * from Beers where Id = ? ;";

        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);
        prepared.setInt(1, id);
        ResultSet result = prepared.executeQuery();

        if (result.next())
            return result.getString("Name");
        return "";

    }

    public String deleteBeerFromResultSet(int id) {
        // Delete a beer with a specific ID from a result set, could just be basic update.
        String beerName = "";
        try {
            ResultSet result = jdbcFacade.executeQueryNavigableUpdatable("select * from Beers where Id = " + id + ";");
            if (result.next()) {
                result.getString("Name");
                result.deleteRow();
            }
        } catch (SQLException e) {
            System.out.println("SQLEX @ deleteBeer");
            e.printStackTrace();
        }
        return beerName;

    }

    public int getBeerCount() {
        ResultSet result = jdbcFacade.executeQuery("select count(Id) from Beers;");
        try {
            if (result.next())
                return result.getInt("count(Id)");
        } catch (SQLException sqle) {
            System.out.println("SQLEX at getBeerCount");
            sqle.printStackTrace();
        }
        return -1;

    }

    public int deleteBeerBy(int brewerId) {
        //Delete beers belonging to specific brewer
        int deletedRows = 0;
        try {
            ResultSet results = jdbcFacade.executeQueryNavigableUpdatable("select * from Beers where" +
                    " BrewerId = " + brewerId + ";");
            while (results.next()) {
                results.deleteRow();
                deletedRows++;
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        return deletedRows;
    }


/* Opdracht 8:*/

    public int getBeersStockByName(String beerName) throws SQLException {
        String sql = "Select * from Beers where `Name` = ?;";

        PreparedStatement prepared = jdbcFacade.prepareStatement(sql);
        prepared.setString(1, beerName);
        ResultSet result = prepared.executeQuery();

        if (result.next()) {
            return result.getInt("Stock");

        }
        return 0;
    }





    public int updateTwoBeersStockWithTransaction
            (String beerName1, String beerName2, int increase) throws SQLException {
        Connection connect = jdbcFacade.getConnection();
        String sql1 = "Update Beers set `Stock` = `Stock` + ? where Name = ?;";

        PreparedStatement prep1 = jdbcFacade.prepareStatement(sql1);
        PreparedStatement prep2 = jdbcFacade.prepareStatement(sql1);

        prep1.setInt(1, increase);
        prep1.setString(2, beerName1);
        prep2.setInt(1, increase);
        prep2.setString(2, beerName2);

        try {
            // only using facade for its connection:

            connect.setAutoCommit(false);
            prep1.executeQuery();
            prep2.executeQuery();
            //connect.rollback();
            connect.commit();
            return increase * 2;

        } catch (SQLException sqle) {
            connect.rollback();
            sqle.printStackTrace();
            return 0;
        }
    }
}
