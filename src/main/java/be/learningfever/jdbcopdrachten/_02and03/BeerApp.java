package be.learningfever.jdbcopdrachten._02and03;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.*;

public class BeerApp {
    private static final String URL = "jdbc:mariadb://multimedi-education.be/javadevt_Antw5";
    private static final String USER = "javadevt_StudAnt";
    private static final String PSW = "2020VJejo";

    public static void main(String[] args) throws SQLException {
        double alcohol;
        String sql = "select * from Beers order by Alcohol DESC limit 20;";

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

         //Unlike in the test OPDRACHT 3 this query has access to the whole database but for some reason it does not connect...
//       try (Connection connect1 = DriverManager.getConnection(URL, USER, PSW)) {
//           System.out.println(showBeerWithId(connect1, 14));
//        } catch (Exception e) {
//           e.printStackTrace();
//       }

        try( //with resources
             Connection connection = DriverManager.getConnection(
                     "jdbc:mariadb://multimedi-education.be/javadevt_Antw5",
                     "javadevt_StudAnt",
                     "2020VJejo")

        ) {
            do {

                System.out.println("Enter alcohol value:");
                alcohol = Double.parseDouble(in.readLine());
                printBeerByVol(connection, alcohol);
            }while(alcohol!=100.0);

            printBeers(connection, sql);

        } catch (Exception e) {

            System.out.println("Connection failed or incorrect input");
            e.printStackTrace();
        }
    }

    public static void printBeerByVol(Connection connection,  double alcohol) throws SQLException {
        String sql = "select * from Beers WHERE Alcohol=" + alcohol +";";

        Statement statement = connection.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY
        );

        ResultSet result = statement.executeQuery(sql);
        printResultSet(result);
    }

    public static void printBeers(Connection connection, String sql) throws SQLException {
        Statement statement = connection.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY
        );

        ResultSet result = statement.executeQuery(sql);
        printResultSet(result);
    }

    public static void printResultSet(ResultSet result) throws SQLException {
        while (result.next()) {
            String beerName = result.getString("Name");
            double alcoholB = result.getDouble("Alcohol");
            double price = result.getDouble("Price");
            System.out.printf("%s %.2f %.2f%n", beerName, alcoholB, price);
        }
    }

    public static String showBeerWithId(Connection connect, int id) throws SQLException {
        String sql = "select * from beers where id = " + id + ";";
        Statement statement = connect.createStatement();
        ResultSet result = statement.executeQuery(sql);

        while(result.next()) {
            return result.getString("Name");

        }
        return "FOUTMELDING: Geen resultaten";

    }

}
