package be.learningfever.jdbcopdrachten._05A;

//Step one: I steadfastly refuse to complete this until I have been shown what a dieren constructor is.

import java.sql.*;

public class BeerData {
    JdbcFacade_Copied jdbcHelper = new JdbcFacade_Copied();
    ResultSet result;
    Connection connect = DriverManager.getConnection(
            "jdbc:mariadb://multimedi-education.be/javadevt_Antw5",
            "javadevt_StudAnt",
            "2020VJejo"
    );

    public BeerData() throws SQLException {
    }

    public static void main(String[] args) throws SQLException {
        BeerData data = new BeerData();
        data.helperApp();

    }

    public void helperApp() {
        try {
            //System.out.println(getBeerById(11)); // This fails to connect method attempts to use jdbc helper.
            System.out.println(getBeersByIdManualConnection(11, connect));
        } catch (RuntimeException | SQLException re) {
            re.printStackTrace();
            System.out.println("All kinds of SQL runtime exceptions too..");
        }

    }

    public String getBeerById(int id) {
        String sql = "select * from beers where id = " + id + ";";
        try {
            result = jdbcHelper.executeQuery(sql);
            return result.getString("Name");
        } catch (SQLException sqle) {
            return "Error: No Beer found with that id";
        }
    }

    public String getBeersByIdManualConnection(int id, Connection connect) throws SQLException {
        String sql = "select * from beers;";
        Statement statement = connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        result = statement.executeQuery(sql);
        System.out.println("NEVER GETS HERE");
        while(result.next()){
            System.out.println("inside while");
        }
        return result.getString("Name");
        }
    }

