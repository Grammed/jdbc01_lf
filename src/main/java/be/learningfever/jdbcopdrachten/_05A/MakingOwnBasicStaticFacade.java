package be.learningfever.jdbcopdrachten._05A;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class MakingOwnBasicStaticFacade { // not implementing auto closeable atm

    public static final int TYPE_STANDARD = ResultSet.TYPE_FORWARD_ONLY;
    public static final int TYPE_SCROLL = ResultSet.TYPE_SCROLL_INSENSITIVE;
    public static final int TYPE_SCROLL_SYNCED = ResultSet.TYPE_SCROLL_SENSITIVE;
    public static final int CONCUR_READ_ONLY = ResultSet.CONCUR_READ_ONLY;
    public static final int CONCUR_UPDATEABLE = ResultSet.CONCUR_UPDATABLE;

    public static final String PROPERTIES_PATH = "src/main/resources/mainInfo.properties";

    // For easy demo from main just one of these at a time
//    private static Properties properties;
//    private static Connection connection;
//    private static Statement statement;
//    private static ResultSet results;
    //private static PreparedStatement preparedStatement;

    public static void main(String[] args) {
        MakingOwnBasicStaticFacade makingOwnBasicFacade = new MakingOwnBasicStaticFacade();
        Properties properties;
        Connection connection;
        Statement statement;
        ResultSet results;
        properties = readProperties_01(PROPERTIES_PATH);
        connection = getConnection_02(properties);
        statement = createStatement_03(connection, TYPE_SCROLL, CONCUR_READ_ONLY);
        results = executeQuery_04(statement, "select * from Beers;");

        while (true) {
            try {
                System.out.println(results.getString("Name"));
                if (!results.next()) break;
            } catch (SQLException sqle) {
                System.out.println("sqle @ result loop print");
                sqle.printStackTrace();
            }
        }




    }

    private static  Properties readProperties_01(String propertiesPath) {
        Properties properties = null;
        try (FileInputStream in = new FileInputStream(propertiesPath)) {
           properties = new Properties();
           properties.load(in);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        } catch (IOException ioException) {
            System.out.println("Issue with FileInputStream (I/O)");
            ioException.printStackTrace();
        }
        return properties;
    }

    private static Connection getConnection_02(Properties properties) {
        Connection connection = null;
        try {
            connection = DriverManager
                    .getConnection(properties.getProperty("jdbc.url"), properties.getProperty("jdbc.user"),
                            properties.getProperty("jdbc.psw"));


        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        return connection;
    }

    private static Statement createStatement_03(Connection connection, int resultSetType, int resultSetConcurent) {
        Statement statement = null;
        try {
            statement = connection.createStatement(resultSetType, resultSetConcurent);
        } catch (SQLException sqle) {
            System.out.println("SQLE thrown @ createStatement_03");
            sqle.printStackTrace();
        }
        return statement;
    }

    private static ResultSet executeQuery_04(Statement statement, String sql)  {
        ResultSet result = null;
        try {
            result = statement.executeQuery(sql);
        } catch (SQLException sqle) {
            System.out.println("sqle @ executeQuery_04");
            sqle.printStackTrace();
        } catch (NullPointerException npe) {
            System.out.println("npe @ executeQuery_04");
            npe.printStackTrace();
        }
        return result;
    }

    // Store statement for later use
//    private static PreparedStatement prepareStatement_Store_Reuse_03a(Statement statement, String sql) {
 //   }

}
