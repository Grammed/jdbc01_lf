package be.learningfever.jdbcopdrachten.opdracht10.exceptions;

public class BeerObjectCreationException extends Exception {

    public BeerObjectCreationException(String message){
        super(message);
    }
}
