package be.learningfever.jdbcopdrachten.opdracht10.beers;

import java.io.Serializable;
import java.util.Objects;

public class Beer implements Serializable{


        private int id;
        private String name;
        private float price;
        private float alcohol;
        private int stock;

        public Beer() {
        }
        public Beer(int id, String name, float price, float alcohol, int stock) {
            this.id = id;
            this.name = name;
            this.price = price;
            this.alcohol = alcohol;
            this.stock = stock;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public float getAlcohol() {
            return alcohol;
        }

        public void setAlcohol(float alcohol) {
            this.alcohol = alcohol;
        }

        public int getStock() {
            return stock;
        }

        public void setStock(int stock) {
            this.stock = stock;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Beer beer = (Beer) o;
            return getId() == beer.getId() &&
                    Double.compare(beer.getPrice(), getPrice()) == 0 &&
                    Double.compare(beer.getAlcohol(), getAlcohol()) == 0 &&
                    getStock() == beer.getStock() &&
                    Objects.equals(getName(), beer.getName());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getId(), getName(), getPrice(), getAlcohol(), getStock());
        }

        @Override
        public String toString() {
            return String.format(
                    "%d - %s : %.2f euro with %.2f Alc.%d in stock",
                    getId(),
                    getName(),
                    getPrice(),
                    getAlcohol(),
                    getStock()
            );
        }
    }




