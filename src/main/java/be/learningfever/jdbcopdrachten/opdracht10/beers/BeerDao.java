package be.learningfever.jdbcopdrachten.opdracht10.beers;

import be.learningfever.jdbcopdrachten.opdracht10.exceptions.BeerObjectCreationException;

import java.sql.SQLException;
import java.util.List;

public interface BeerDao {

    Beer getBeerById(int id) throws SQLException, BeerObjectCreationException;
    void updateBeer(Beer beer, int flag) throws SQLException; //added flag for reset and object testing without Mockito
    List<Beer> getBeersByAlcohol(float alcohol) throws SQLException, BeerObjectCreationException;
    List<Beer> getBeersByMainName(String name) throws SQLException, BeerObjectCreationException;
}
