package be.learningfever.jdbcopdrachten.opdracht10.daos;

import be.learningfever.jdbcopdrachten.opdracht10.JdbcFacade;
import be.learningfever.jdbcopdrachten.opdracht10.beers.Beer;
import be.learningfever.jdbcopdrachten.opdracht10.beers.BeerDao;
import be.learningfever.jdbcopdrachten.opdracht10.exceptions.BeerObjectCreationException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StandardBeerDao implements BeerDao {
    JdbcFacade jdbcFacade;

    StandardBeerDao() {
        this.jdbcFacade = new JdbcFacade();
    }

    StandardBeerDao(JdbcFacade jdbcFacade) {
        this.jdbcFacade = jdbcFacade;
    }

    public JdbcFacade getJdbcFacade() {
        return jdbcFacade;
    }

    public void setJdbcFacade(JdbcFacade jdbcFacade) {
        this.jdbcFacade = jdbcFacade;
    }

    @Override
    public Beer getBeerById(int id) throws SQLException, BeerObjectCreationException {
        String sql = ("select * from Beers where Id = ?;");
        Beer beer = new Beer();
        try {
            PreparedStatement preparedStatement = jdbcFacade.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet results = preparedStatement.executeQuery();

            if (results.next()) {
                beer.setId(results.getInt("Id"));
                beer.setName(results.getString("Name"));
                beer.setPrice(results.getFloat("Price"));
                beer.setAlcohol(results.getFloat("Alcohol"));
                beer.setStock(results.getInt("Stock"));
                return beer;
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            throw new BeerObjectCreationException("Issue creating Beer Object");
        }
        return beer;
    }

    @Override
    public void updateBeer(Beer beer, int flag) throws SQLException {

        String sql1 = "Update Beers set `Name` = ? where id = ?;";
        String sql2 = "Update Beers set `Price` = ? where id = ?;";
        String sql3 = "Update Beers set `Alcohol` = ? where id = ?;";
        String sql4 = "Update Beers set `Stock` = ? where id = ?;";

        PreparedStatement prep1 = jdbcFacade.prepareUpdateableStatement(sql1);
        PreparedStatement prep2 = jdbcFacade.prepareUpdateableStatement(sql2);
        PreparedStatement prep3 = jdbcFacade.prepareUpdateableStatement(sql3);
        PreparedStatement prep4 = jdbcFacade.prepareUpdateableStatement(sql4);


        // Show setting capability
        if (flag == 1) {
            prep1.setString(1, "Gadax");
            prep1.setInt(2, beer.getId());
            prep1.executeUpdate();
            prep2.setFloat(1, 12.22f);
            prep2.setInt(2, beer.getId());
            prep2.executeUpdate();
            prep3.setFloat(1, 2.0f);
            prep3.setInt(2, beer.getId());
            prep3.executeUpdate();
            prep4.setInt(1, 1000);
            prep4.setInt(2, beer.getId());
            prep4.executeUpdate();
        }

        // Reset
        if (flag == 0) {
            prep1.setString(1, "Akila pilsener");
            prep1.setInt(2, beer.getId());
            prep1.executeUpdate();
            prep2.setFloat(1, 2.55f);
            prep2.setInt(2, beer.getId());
            prep2.executeUpdate();
            prep3.setFloat(1, 5.0f);
            prep3.setInt(2, beer.getId());
            prep3.executeUpdate();
            prep4.setInt(1, 100);
            prep4.setInt(2, beer.getId());
            prep4.executeUpdate();
        }
    }


    @Override
    public List<Beer> getBeersByAlcohol(float alcohol) throws SQLException, BeerObjectCreationException {
        List<Beer> beersAtAlcoholVolume = new ArrayList<>();
        String sql = "select * from Beers where `Alcohol` = ?;";

        PreparedStatement preparedStatement = jdbcFacade.prepareStatement(sql);
        preparedStatement.setFloat(1, alcohol);

        ResultSet results = preparedStatement.executeQuery();

        while(results.next()) {
            beersAtAlcoholVolume.add(getBeerById(results.getInt("Id")));
        }
        return beersAtAlcoholVolume;
    }

    @Override
    public List<Beer> getBeersByMainName(String name) throws SQLException, BeerObjectCreationException {
        List<Beer> beersByBrand = new ArrayList<>();
        String sql = "select * from Beers where `Name` like ?;";

        PreparedStatement preparedStatement = jdbcFacade.prepareStatement(sql);
        preparedStatement.setString(1, name);
        ResultSet results = preparedStatement.executeQuery();

        while(results.next()) {
            beersByBrand.add(getBeerById(results.getInt("Id")));
        }
        return beersByBrand;
    }
}