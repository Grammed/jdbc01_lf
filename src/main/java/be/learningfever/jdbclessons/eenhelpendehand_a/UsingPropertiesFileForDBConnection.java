package be.learningfever.jdbclessons.eenhelpendehand_a;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class UsingPropertiesFileForDBConnection {

    public static void main(String[] args) throws IOException {
        try (FileInputStream in = new FileInputStream("src/main/resources/mainInfo.properties")) {
            Properties props = new Properties();
            props.load(in);
            String igloo = props.getProperty("jdbc.url");
            System.out.println(igloo);
        } catch (IOException ioe ) {
            ioe.printStackTrace();
        }
    }
}
