package be.learningfever.jdbclessons;

import java.sql.*;

public class FirstConnectionAndQuery {
    public static void main(String[] args) throws SQLException {

        String sql = "select * from Beers limit 20;";
        System.out.println(sql);
        try( //with resources
                Connection connection = DriverManager.getConnection(
                        "jdbc:mariadb://multimedi-education.be/javadevt_Antw5",
                        "javadevt_StudAnt",
                        "2020VJejo");

                ) {

            Statement statement = connection.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,  //Scroll direction
                    ResultSet.CONCUR_READ_ONLY          // Can it be updated?
            );

            System.out.println("Connection OK");
            ResultSet result = statement.executeQuery(sql);
            System.out.println(result); //Navigable result sets are called 'Scrollable".
            while (result.next()) {
                String beerName = result.getString("Name");
                double alcohol = result.getDouble("Alcohol");
                double price = result.getDouble("Price");
                System.out.printf("%s %.2f %.2f%n", beerName, alcohol, price);
            }

        } catch (Exception e) {

            System.out.println("Connection failed");
            e.printStackTrace();
        }
    }
}
