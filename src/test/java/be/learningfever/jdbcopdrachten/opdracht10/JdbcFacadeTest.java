package be.learningfever.jdbcopdrachten.opdracht10;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class JdbcFacadeTest {
    JdbcFacade jdbcFacade = new JdbcFacade();

    @Test
    void testGetProperties() {
        System.out.println(jdbcFacade.getProperties());
    }

    @Test
    void getUrl() {
    }

    @Test
    void getUser() {
    }

    @Test
    void getPsw() {
    }

    @Test
    void testGetConnection() {
        System.out.println(jdbcFacade.getConnection());

    }

    @Test
    void testGetStatement() {
        System.out.println(jdbcFacade.getStatement());
    }

    @Test //and prepare statement ?'s cannot be used to insert tables.
    void testExecuteQuery() throws SQLException {
        //String table = "`A_Product`"; // This was failing

        //String sql = "Select * from ?;";
        String sql = "Select * from `A_Product`;";

        //PreparedStatement prep1 = jdbcFacade.prepareStatement(sql);
        //prep1.setString(1, table);
        //ResultSet result = prep1.executeQuery();

        Statement stat = jdbcFacade.getStatement();
        ResultSet result = stat.executeQuery(sql);


        while (result.next()) {
            System.out.println(result.getString(1) + " " + result.getString("Name"));
        }


    }

    @Test
    void testExecuteUpdate() {

        String sql1 = "UPDATE `javadevt_Antw5`.`A_Product` SET `Description` = 'dingbat' WHERE `InternalProductID` = '2'";

        jdbcFacade.executeUpdate(sql1);



    }

}