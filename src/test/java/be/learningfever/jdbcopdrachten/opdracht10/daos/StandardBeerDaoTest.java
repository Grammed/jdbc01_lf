package be.learningfever.jdbcopdrachten.opdracht10.daos;

import be.learningfever.jdbcopdrachten.opdracht10.JdbcFacade;
import be.learningfever.jdbcopdrachten.opdracht10.beers.Beer;
import be.learningfever.jdbcopdrachten.opdracht10.exceptions.BeerObjectCreationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class StandardBeerDaoTest {

    StandardBeerDao standardBeerDao = new StandardBeerDao();
    Beer beer = new Beer();
    Beer constructorCreated = new Beer(18, "Akila pilsener", 2.55f, 5.00f, 100);
    JdbcFacade jdbcFacadeForTest = standardBeerDao.getJdbcFacade();

    /*@Test
    void TestgetBeerById() throws SQLException, BeerObjectCreationException {
        int id = 18;
        String expected = "18 - Akila pilsener : 2,55 euro with 5,00 Alc. 100 in stock";
        Beer returnedBeerObject = standardBeerDao.getBeerById(id);
        String actual = (standardBeerDao.getBeerById(id).toString());

       // System.out.println(standardBeerDao.getBeerById(18));
        //Note this is a very poor test as any change to Beers toString will screw it up. I will revisit Mockito for this.
        assertEquals(expected, actual);
    }*/

    @Test
    void testGetBeerById() throws SQLException, BeerObjectCreationException {
        Beer returnedByDaoMethod = standardBeerDao.getBeerById(18);
        System.out.println(returnedByDaoMethod);
        assertEquals(constructorCreated, returnedByDaoMethod);


        /*assertThrows(BeerObjectCreationException.class, () -> {
            standardBeerDao.getBeerById(-1);
        });*/

    }

    @Test
    void TestUpdateBeer() throws SQLException, BeerObjectCreationException {
        //Update the constructorCreated beer (id 18) by passing its object and 'update' flag 1
      standardBeerDao.updateBeer(constructorCreated,1); // remember the method just gets id from object
        //Same beer id but with updated info object:
       Beer updatedBeer = standardBeerDao.getBeerById(18);

        System.out.println(constructorCreated);
        System.out.println(updatedBeer  );
        assertNotEquals(constructorCreated, updatedBeer);

        //Reset the beer to the default values in the method by passing its object and 'reset' flag 0
        standardBeerDao.updateBeer(constructorCreated, 0);
        updatedBeer = standardBeerDao.getBeerById(18);
        assertEquals(constructorCreated, updatedBeer);


    }

    @Test
    void testGetBeersByAlcohol() throws BeerObjectCreationException, SQLException {
        Float alcVol = 3.0f;
        int countAtVol = -1;
        List<Beer> returned = standardBeerDao.getBeersByAlcohol(alcVol);
        String sql = "select count(`Id`) as BeersAtThisVolume from Beers where `Alcohol` = ?;";

        PreparedStatement preparedStatement = jdbcFacadeForTest.prepareStatement(sql);
        preparedStatement.setFloat(1,alcVol);
        ResultSet result = preparedStatement.executeQuery();

        if(result.next()) {
          countAtVol = result.getInt("BeersAtThisVolume");
            //System.out.println(countAtVol);
        }

        for (Beer beer: returned)
            System.out.println(beer);

        assertFalse(returned.isEmpty());
        System.out.println("Returned List Size " + returned.size() + " Independent SQL count: " + countAtVol);
        assertTrue(returned.size() == countAtVol);

    }

    @Test
    void getBeersByName() throws BeerObjectCreationException, SQLException {
        String brandWildSearch = "%Affligem%";
        int countContainingName = -1;
        List<Beer> returned = standardBeerDao.getBeersByMainName(brandWildSearch);
        String sql = "select count(`Id`) as CountContainingWildSearch from Beers where `Name` like ?;";

        PreparedStatement preparedStatement = jdbcFacadeForTest.prepareStatement(sql);
        preparedStatement.setString(1,brandWildSearch);
        ResultSet result = preparedStatement.executeQuery();

        if(result.next()) {
            countContainingName = result.getInt("CountContainingWildSearch");
        }

        for (Beer beer: returned)
            System.out.println(beer);

        assertFalse(returned.isEmpty());
        System.out.println("Returned List Size " + returned.size() + " Independent SQL count: " + countContainingName);
        assertTrue(returned.size() == countContainingName);
    }
}