package be.learningfever.jdbcopdrachten._05A;

import org.junit.jupiter.api.*;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class BeerDataTest {

    BeerData data;
    @BeforeEach
    void init() throws SQLException {
        BeerData data = new BeerData();
    }


    @Test
    void TestGetBeerById() {
        String expected = "Alfri";
        String actual = data.getBeerById(26);
        assertEquals(expected, actual, "The get beer by ID method should return the correct beer");
    }
}