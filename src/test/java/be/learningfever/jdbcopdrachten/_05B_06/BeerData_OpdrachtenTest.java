package be.learningfever.jdbcopdrachten._05B_06;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BeerData_OpdrachtenTest {
    BeerData_Opdrachten beerData_Opdrachten = new BeerData_Opdrachten();


    @Test
    void testGetBeerById() throws SQLException {
        String expected = "Id 4, Name AOC, BrewerId 104, CategoryId 18, " +
                "Price 2,75, Stock 100, Alcohol 7,00, Version 0, Image null";
        String unexpected = "null";
        String actual = beerData_Opdrachten.getBeerById(4);
        assertEquals(expected, actual);
        assertNotEquals(unexpected, actual);
        System.out.println(actual);
    }


    @Test
    void testGetBeerByIdAndChangePrice() throws SQLException {
        float originalPrice = beerData_Opdrachten.getBeerPriceById(5);
        float newPrice = originalPrice + 0.01f;
        int returnedIntValue = beerData_Opdrachten.getBeerByIdAndChangePrice(5, newPrice);
        float updatedPrice = beerData_Opdrachten.getBeerPriceById(5);
        System.out.println("Original: " + originalPrice);
        System.out.println("updated: " + updatedPrice);
        System.out.println("returned int value: " + returnedIntValue);
        assertEquals(newPrice, updatedPrice);
        // Assert 1 row effected
        assertEquals(1, returnedIntValue);
    }

    @Test
    void testPrintBeersByAlcoholVolume() throws SQLException {
        ResultSet results = beerData_Opdrachten.printBeersByAlcoholVolume(6.00f);
        while (results.next()) {
            System.out.print(results.getString("Name"));
            System.out.print(" " + results.getString("Price") + "\n");
        }
    }


    @Test
    void testGetBeersByAlcoholAndChangePrice() throws SQLException {

        double currentAverage = beerData_Opdrachten.getAveragePriceByAlcoholVolume(4.0f);
        double scale = Math.pow(10, 2);
        // an actual use for this truncated version increments
        float truncatedFloat = (float) (Math.round(currentAverage * scale) / scale);
        float increasedPrice = truncatedFloat + 0.01f;
        int actual = beerData_Opdrachten.getBeersByAlcoholAndChangePrice(4.0, increasedPrice);
        double afterAverage = beerData_Opdrachten.getAveragePriceByAlcoholVolume(4.0f);

        assertTrue(afterAverage > currentAverage);
        System.out.println(afterAverage - truncatedFloat);
    }

    @Test
    void getBeersBySubNameAndIncrementStock() throws SQLException {
        int currentStock = beerData_Opdrachten.getBeersStockBySubName("%ars%");
        System.out.println(currentStock);
        int rowsAffected = beerData_Opdrachten.getBeersBySubNameAndIncrementStock("%ars%", 1);
        int updatedStock = beerData_Opdrachten.getBeersStockBySubName("%ars%");
        assertEquals(currentStock + rowsAffected, updatedStock);
    }

    @Test
    void getAveragePriceByAlcoholVolume() throws SQLException {
        Double average = beerData_Opdrachten.getAveragePriceByAlcoholVolume(5.0f);
        assertNotEquals(0.0, average);
    }

    @Test
    void testGetBeersStockBySubName() throws SQLException {
        int returnedSum = beerData_Opdrachten.getBeersStockBySubName("%elt%");
        assertEquals(400, returnedSum);
    }


    /*
     *))))))))))))))))))))))(((((((((((((((((((((((((((((
     *))))))))))))))))))OPDRACHT TTT 06((((((((((((((((((
     *))))))))))))))))))))))(((((((((((((((((((((((((((((
     * */

    @Test
        //This is a basic insert statement, that doesn't follow opdracht to the letter (could be clearer).
    void testAddATestBeer() throws SQLException {
        //Originally "Insert into beer" but this shows nothing new, this method as described is more like "copy beer".
        int originalIdMax = beerData_Opdrachten.getCurrentMaxId();
        beerData_Opdrachten.addATestBeer();
        int IdMaxAfterAddition = beerData_Opdrachten.getCurrentMaxId();
        int actual = IdMaxAfterAddition - originalIdMax;
        System.out.println("MaxIDB4: " + originalIdMax + "  MaxIDAfter: " + IdMaxAfterAddition);
        assertEquals(1, actual);
    }

    @Test
    void testGetBeerNameById() throws SQLException {
        String expected = "Sas export";
        String actual = beerData_Opdrachten.getBeerNameById(1162);
        assertEquals(expected, actual);
    }

    @Test
    void testCopyBeer() throws SQLException {
        int beerId = 1066;
        String expected = "Copy of " + (beerData_Opdrachten.getBeerNameById(beerId));
        int originalIdMax = beerData_Opdrachten.getCurrentMaxId();
        String beerCreated = beerData_Opdrachten.copyBeer(beerId);
        int IdMaxAfterAddition = beerData_Opdrachten.getCurrentMaxId();
        int actual = IdMaxAfterAddition - originalIdMax;
        System.out.println("-----------------------\n\nBEER CREATED " + beerCreated);
        assertEquals(1, actual);
        assertEquals(expected, beerCreated);
    }

    @Test
    void testDeleteBeerFromResultSet() {

        int beerToDelete = 1561;
        int originalCount = beerData_Opdrachten.getBeerCount();
        String deletedBeer = beerData_Opdrachten.deleteBeerFromResultSet(beerToDelete);
        int countAfterDeletion = beerData_Opdrachten.getBeerCount();
        System.out.println(originalCount + "   " + countAfterDeletion);
        int actual = originalCount - countAfterDeletion;
        System.out.println("-----------------------\n\nBEER Deleted " + deletedBeer);
        assertEquals(1, actual);
    }

    @Test
    void testDeleteBeerBy() {
        int brewerId = 2;
        int initialRows = beerData_Opdrachten.getBeerCount();
        int deletedRows = beerData_Opdrachten.deleteBeerBy(brewerId);
        int finalRows = beerData_Opdrachten.getBeerCount();
        assertEquals(finalRows, initialRows - deletedRows);
        assertTrue(finalRows < initialRows);
    }

    @Test
    void testGetCurrentMaxId() throws SQLException {

    }

    @Test
    void testGetBeersStockByName() throws SQLException {
        String beerName = "Aldegonde brune";
        int expected = 150;
        int actual = beerData_Opdrachten.getBeersStockByName("Aldegonde brune");
        assertEquals(expected, actual);
    }


    @Test
    void testUpdateTwoBeersStockWithTransaction() throws SQLException {
        String beer1 = "Bios bruin";
        String beer2 = "Bios blond";
        int beer1Stock = beerData_Opdrachten.getBeersStockByName(beer1);
        int beer2Stock = beerData_Opdrachten.getBeersStockByName(beer2);
        int increment = 5;
        int expected = increment * 2 + beer1Stock + beer2Stock;

        beerData_Opdrachten.updateTwoBeersStockWithTransaction(beer2, beer1, increment);

        int actual = beerData_Opdrachten.getBeersStockByName(beer1) +
                beerData_Opdrachten.getBeersStockByName(beer2);

        assertEquals(expected, actual);
    }


//        int currentStock = beerData_Opdrachten.getBeersStockBySubName("%ars%");
//        System.out.println(currentStock);
//        int rowsAffected = beerData_Opdrachten.getBeersBySubNameAndIncrementStock("%ars%", 1);
//        int updatedStock = beerData_Opdrachten.getBeersStockBySubName("%ars%");
//        assertEquals(currentStock + rowsAffected, updatedStock);
}
