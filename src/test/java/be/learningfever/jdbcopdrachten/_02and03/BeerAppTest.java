package be.learningfever.jdbcopdrachten._02and03;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;

class BeerAppTest {

@BeforeEach
public void init() throws SQLException, Exception {
    try (
            Connection connect = DriverManager.getConnection(
                    "jdbc:hsqldb:mem:mymemdb",
                    "sa",
                    ""
            );
            // Create InMemory DataBase for testing
            Statement statement = connect.createStatement();
            Reader schemaReader = new BufferedReader(new FileReader("src/test/resources/schema.sql"));
            Reader dataReader = new BufferedReader(new FileReader("src/test/resources/data.sql"));
            ) {
        ScriptRunner runner = new ScriptRunner(connect);
        runner.runScript(schemaReader);
        runner.runScript(dataReader);
    }
}
@Test
    void basisTest() throws SQLException {
    Connection connect = DriverManager.getConnection(
            "jdbc:hsqldb:mem:mymemdb",
            "sa",
            ""
    );

    String testWaarde = BeerApp.showBeerWithId(connect, 1);
    String failendeTestWaarde = BeerApp.showBeerWithId(connect,3);
    String verwachtWaarde = "TestBeer";
    String failendeVerwachtWaarde = "FOUTMELDING: Geen resultaten";
    assertEquals(verwachtWaarde, testWaarde);
    assertEquals(failendeVerwachtWaarde, failendeTestWaarde);
}

}