package be.learningfever.jdbclessons.opdracht04;

import org.junit.jupiter.api.*;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class JdbcFacadeTest {
    JdbcFacade jdbcFacade = new JdbcFacade();

// NOT required
//    @BeforeAll
//    void init(TestInfo testInfo, TestReporter testReporter) throws SQLException {
//        jdbcFacade = new JdbcFacade();
//        //Connection connect = DriverManager.getConnection(jdbcFacade.getUrl(), jdbcFacade.getUser(), jdbcFacade.getPsw());
//    }

    @Test
    void testGetProperties() {

    }

    @Test
    void TestGetUrl() {
        String expected = "jdbc:mariadb://multimedi-education.be/javadevt_Antw5";
        String actual = jdbcFacade.getUrl();
        assertEquals(expected, actual, "The value should be the same as is saved in the resources file.");
    }

    @Test
    void TestGetUser() {
        String expected = "javadevt_StudAnt";
        String actual = jdbcFacade.getUser();
        assertEquals(expected, actual, "The value should be the same as is saved in the resources file.");
    }

    @Test
    void TestGetPsw() {
        String expected = "2020VJejo";
        String actual = jdbcFacade.getPsw();
        assertEquals(expected, actual, "The value should be the same as is saved in the resources file.");
    }

    @Test
    void TestGetConnection() throws SQLException {
        int count = 0;
        Connection connect = DriverManager.getConnection(jdbcFacade.getUrl(), jdbcFacade.getUser(), jdbcFacade.getPsw());
        Statement statement = connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet result = statement.executeQuery("select * from Beers where Id = 26;");
        while (result.next()) {

            if ("Alfri".equals(result.getString("Name"))) {
                System.out.println(result.getString("Name"));
            }
            System.out.println("GetConnection results" + " " + count);
            count++;
        }
    }

    @Test
    void testExecuteQueryNavigable() throws SQLException {
        //Connection connect = DriverManager.getConnection(jdbcFacade.getUrl(), jdbcFacade.getUser(), jdbcFacade.getPsw());
        ResultSet result = jdbcFacade.executeQueryNavigable("select * from Beers where Id = 4;");
        String expected = "AOC";
        String actual = "";
        // ResultSet cursor returned positioned BEFORE the first row.
        if(result.next())

        actual = result.getString("Name");

        assertEquals(expected, actual, "AOC retrieved");
    }

    @Test
    void testExecuteQuery() throws SQLException {
        //Connection connect = DriverManager.getConnection(jdbcFacade.getUrl(), jdbcFacade.getUser(), jdbcFacade.getPsw());
        ResultSet result = jdbcFacade.executeQuery("select * from Beers where Id = 4;");
        String expected = "AOC";
        String actual = "";
        // ResultSet cursor returned positioned BEFORE the first row.
        if(result.next())

            actual = result.getString("Name");

        assertEquals(expected, actual, "AOC retrieved");

    }

    @Test
    void testExecuteUpdate() throws SQLException {
        //Connection connect = DriverManager.getConnection(jdbcFacade.getUrl(), jdbcFacade.getUser(), jdbcFacade.getPsw());
        jdbcFacade.executeUpdate("update Beers set `Name` = \'AOC\' where id = 4;");

    }

}