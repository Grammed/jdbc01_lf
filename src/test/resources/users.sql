-- hsql for User table changed dialect


CREATE TABLE IF NOT EXISTS User (
                                        UserInternal_Id_ INTEGER GENERATED BY DEFAULT AS IDENTITY
                                            (START WITH 1) PRIMARY KEY,
                                        UserStore INTEGER,
                                        UserAccessLevel INTEGER,
                                        UserLogInName VARCHAR(100),
                                        UserFirstName VARCHAR(50),
                                        UserLastName VARCHAR(50),
)