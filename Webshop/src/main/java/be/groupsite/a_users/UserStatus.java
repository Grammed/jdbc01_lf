package be.groupsite.a_users;

public class UserStatus {
    private String userInternal_Id;
    private boolean userActive;
    private String userPassword;
    //private calendar date created_at, not implementing yet.


    public UserStatus(String userInternal_Id, boolean userActive, String userPassword) {
        this.userInternal_Id = userInternal_Id;
        this.userActive = userActive;
        this.userPassword = userPassword;
    }

    public String getUserInternal_Id() {
        return userInternal_Id;
    }

    public boolean isUserActive() {
        return userActive;
    }

    public void setUserActive(boolean userActive) {
        this.userActive = userActive;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserStatus that = (UserStatus) o;

        if (userActive != that.userActive) return false;
        if (!userInternal_Id.equals(that.userInternal_Id)) return false;
        return userPassword.equals(that.userPassword);
    }

    @Override
    public int hashCode() {
        int result = userInternal_Id.hashCode();
        result = 31 * result + (userActive ? 1 : 0);
        result = 31 * result + userPassword.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserStatus{" +
                "userInternal_Id='" + userInternal_Id + '\'' +
                ", userActive=" + userActive +
                ", userPassword='" + userPassword + '\'' +
                '}';
    }
}
