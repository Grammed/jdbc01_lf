package be.groupsite.a_users;

public enum UserType {
    SYSTEM_ADMIN (1),
    STORE_MANAGER (2),
    STORE_OPERATIVE(3),
    CUSTOMER (4);


    private final int level;

    UserType(int level) {
        this.level = level;
    }

    public int level() {
        return level;
    }
}
