package be.groupsite.a_users;

public interface UserAdminLevelDao {

    UserAdminLevel createUserAdminLevel(UserAdminLevel userAdminLevel);
    UserAdminLevel createUserAdminLevel(UserType userType);
}
