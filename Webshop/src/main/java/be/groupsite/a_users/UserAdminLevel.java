package be.groupsite.a_users;

// Done == encapsulated with Dao and removal of setters? First time I have done this.

public  class UserAdminLevel{
    private UserType userType;
    private boolean addUsers;
    private boolean addStores;
    private boolean addProducts;
    private boolean retireProducts;
    private boolean changeOrderStatus;
    private boolean modifyProducts;
    private boolean productOverviewBackend;
    private boolean storeOverview;
    private boolean productOverviewFrontend;
    private boolean manageMultiStoreBasket;
    private boolean checkout;
    private boolean viewOrderHistory;
    private boolean viewOwnOrderHistory;

    public UserAdminLevel(UserType userType) {
        this.userType = userType;
        this.addUsers = false;
        this.addStores = false;
        this.addProducts = false;
        this.retireProducts = false;
        this.changeOrderStatus = false;
        this.modifyProducts = false;
        this.productOverviewBackend = false;
        this.storeOverview = false;
        this.productOverviewFrontend = false;
        this.manageMultiStoreBasket = false;
        this.checkout = false;
        this.viewOrderHistory = false;
        this.viewOwnOrderHistory = false;
    }

    public UserAdminLevel(UserType userType, boolean addUsers, boolean addStores,
                          boolean addProducts, boolean retireProducts, boolean changeOrderStatus,
                          boolean modifyProducts, boolean productOverviewBackend, boolean storeOverview,
                          boolean productOverviewFrontend, boolean manageMultiStoreBasket, boolean checkout,
                          boolean viewOrderHistory, boolean viewOwnOrderHistory) {
        this.userType = userType;
        this.addUsers = addUsers;
        this.addStores = addStores;
        this.addProducts = addProducts;
        this.retireProducts = retireProducts;
        this.changeOrderStatus = changeOrderStatus;
        this.modifyProducts = modifyProducts;
        this.productOverviewBackend = productOverviewBackend;
        this.storeOverview = storeOverview;
        this.productOverviewFrontend = productOverviewFrontend;
        this.manageMultiStoreBasket = manageMultiStoreBasket;
        this.checkout = checkout;
        this.viewOrderHistory = viewOrderHistory;
        this.viewOwnOrderHistory = viewOwnOrderHistory;
    }

    public UserType getUserType() {
        return userType;
    }

    public boolean isAddUsers() {
        return addUsers;
    }

    public boolean isAddStores() {
        return addStores;
    }

    public boolean isAddProducts() {
        return addProducts;
    }

    public boolean isRetireProducts() {
        return retireProducts;
    }

    public boolean isChangeOrderStatus() {
        return changeOrderStatus;
    }

    public boolean isModifyProducts() {
        return modifyProducts;
    }

    public boolean isProductOverviewBackend() {
        return productOverviewBackend;
    }

    public boolean isStoreOverview() {
        return storeOverview;
    }

    public boolean isProductOverviewFrontend() {
        return productOverviewFrontend;
    }

    public boolean isManageMultiStoreBasket() {
        return manageMultiStoreBasket;
    }

    public boolean isCheckout() {
        return checkout;
    }

    public boolean isViewOrderHistory() {
        return viewOrderHistory;
    }

    public boolean isViewOwnOrderHistory() {
        return viewOwnOrderHistory;
    }

    @Override
    public String toString() {

        return String.format(
                "UserType %s,  AddUsers=%b, AddStores=%b, AddProducts=%b,%nRetireProducts=%b," +
                        " ChangeOrderStatus=%b, ModifyProducts=%b, %nPOverviewBack=%b, StoreOverview=%b," +
                        " POverviewFront=%b, %nManageMultiStoreBasket=%b, Checkout=%b, ViewOrderHistory=%b," +
                        "%nViewOwnOrderHistory=%b",
                getUserType(),
                isAddUsers(),
                isAddStores(),
                isAddProducts(),
                isRetireProducts(),
                isChangeOrderStatus(),
                isModifyProducts(),
                isProductOverviewBackend(),
                isStoreOverview(),
                isProductOverviewFrontend(),
                isManageMultiStoreBasket(),
                isCheckout(),
                isViewOrderHistory(),
                isViewOwnOrderHistory()
        );
    }
}
