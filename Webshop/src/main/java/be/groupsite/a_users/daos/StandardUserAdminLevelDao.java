package be.groupsite.a_users.daos;

//Sets the flags for all activities to match those currently int the database.
// Now with far better encapsulation, the an instance of UserAdminLevel cannot have its fields set.
// Todo maybe make fields final and only have one way to construct would need to investigate though

import be.groupsite.JdbcFacade;
import be.groupsite.a_users.UserAdminLevel;
import be.groupsite.a_users.UserAdminLevelDao;
import be.groupsite.a_users.UserType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;

public class StandardUserAdminLevelDao implements UserAdminLevelDao {
    JdbcFacade jdbcFacade = new JdbcFacade();

    @Override
    public UserAdminLevel createUserAdminLevel(UserAdminLevel userAdminLevel) {
        UserType makeType = userAdminLevel.getUserType();
        int userAccessLevel =  makeType.level();
        UserAdminLevel populatedUserAdminLevel;

        if ( makeType == UserType.CUSTOMER && userAccessLevel == 4)
           populatedUserAdminLevel = createUserAdminLevel(UserType.CUSTOMER);
        else if ( makeType == UserType.STORE_OPERATIVE && userAccessLevel == 3)
            populatedUserAdminLevel = createUserAdminLevel(UserType.STORE_OPERATIVE);
        else if ( makeType == UserType.STORE_MANAGER && userAccessLevel == 2)
            populatedUserAdminLevel = createUserAdminLevel(UserType.STORE_MANAGER);
        else
            populatedUserAdminLevel = createUserAdminLevel(UserType.SYSTEM_ADMIN);

        return populatedUserAdminLevel;
    }

    public UserAdminLevel createUserAdminLevel(UserType passedUserType) {
        if (passedUserType == null)
            throw new InputMismatchException();

        UserAdminLevel populated = null;
        String sql = "select * from A_UserAdminLevel where UserAccessLevel = ?;";
        try {
            PreparedStatement prep = jdbcFacade.prepareStatement(sql);
            prep.setInt(1, 1);
            ResultSet result = prep.executeQuery();

            if (result.next()) {
                boolean addUsers = result.getBoolean("AddUsers");
                boolean addStores = result.getBoolean("AddStores");
                boolean addProducts = result.getBoolean("AddProducts");
                boolean retireProducts = result.getBoolean("RetireProducts");
                boolean changeOrderStatus = result.getBoolean("ChangeOrderStatus");
                boolean modifyProducts = result.getBoolean("ModifyProducts");
                boolean productOverviewBackend = result.getBoolean("ProductOverviewBackend");
                boolean storeOverview = result.getBoolean("StoreOverview");
                boolean productOverviewFrontend = result.getBoolean("ProductOverviewFrontend");
                boolean manageMultiStoreBasket = result.getBoolean("ManageMultiStoreBasket");
                boolean checkout = result.getBoolean("Checkout");
                boolean viewOrderHistory = result.getBoolean("ViewOrderHistory");
                boolean viewOwnOrderHistory = result.getBoolean("ViewOwnOrderHistory");

                populated = new UserAdminLevel(passedUserType, addUsers, addStores, addProducts, retireProducts,
                        changeOrderStatus, modifyProducts, productOverviewBackend, storeOverview,
                        productOverviewFrontend, manageMultiStoreBasket, checkout, viewOrderHistory,
                        viewOwnOrderHistory);

            }

        } catch (SQLException sqle) {
            System.out.println("@createSystemAdmin");
            sqle.printStackTrace();
        }
        return populated;
    }
}
