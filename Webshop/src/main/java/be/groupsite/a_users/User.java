package be.groupsite.a_users;

public class User {
    private UserAdminLevel userAdminLevel;
    private UserStatus userStatus;
    private int userStore;
    private int userAccessLevel; //not needed as UAL is part of user obj
    private String userLogInName;
    private String userFirstName;
    private String userLastName;
    private String userStreet;
    private String userPostcode;
    private String userTown;
    //Have not included date yet.


    public User(UserAdminLevel userAdminLevel, UserStatus userStatus,
                int userStore, int userAccessLevel, String userLogInName,
                String userFirstName, String userLastName, String userStreet,
                String userPostcode, String userTown) {
        this.userAdminLevel = userAdminLevel;
        this.userStatus = userStatus;
        this.userStore = userStore;
        this.userAccessLevel = userAccessLevel;
        this.userLogInName = userLogInName;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userStreet = userStreet;
        this.userPostcode = userPostcode;
        this.userTown = userTown;
    }
}
