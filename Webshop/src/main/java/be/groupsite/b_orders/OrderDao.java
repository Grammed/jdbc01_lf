package be.groupsite.b_orders;

import java.util.List;

public interface OrderDao {

    Order getOrderById(int id);
    void updateOrder(Order order);
    List<Order> getOrdersByUser(int id);

}
