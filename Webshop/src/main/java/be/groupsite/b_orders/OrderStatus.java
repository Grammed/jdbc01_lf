package be.groupsite.b_orders;
public enum OrderStatus {
    BASKET_ONLY (1),
    PAID_AWAIT_CONFIRMATION (2),
    PAID (3),
    READY_FOR_PACKING (4),
    PACKED (5),
    DESPATCHED (6),
    ISSUE_ON_HOLD (7)
    ;

    private final int statusCode;

    OrderStatus(int statusCode) {
        this.statusCode = statusCode;
    }
}
