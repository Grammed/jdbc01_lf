package be.groupsite.b_orders;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Order implements Serializable {
    private int orderId;
    private int userInternalId;
    private OrderStatus orderStatus;
    private List<OrderItem> orderItemList;

    public Order(){};

    public Order(int orderId, int userInternalId, OrderStatus orderStatus) {
        this.orderId = orderId;
        this.userInternalId = userInternalId;
        this.orderStatus = orderStatus;
        this.orderItemList = new ArrayList<>();
    }

    public Order(int orderId, int userInternalId, OrderStatus orderStatus,
                    List arrayList) {
        this.orderId = orderId;
        this.userInternalId = userInternalId;
        this.orderStatus = orderStatus;
        this.orderItemList = arrayList;
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getUserInternalId() {
        return userInternalId;
    }

    public void setUserInternalId(int userInternalId) {
        this.userInternalId = userInternalId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (orderId != order.orderId) return false;
        if (userInternalId != order.userInternalId) return false;
        return orderStatus == order.orderStatus;
    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + userInternalId;
        result = 31 * result + orderStatus.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format(
                "Order %d belongs to User %d, its status is: %S",
                getOrderId(),
                getUserInternalId(),
                getOrderStatus()
                );
    }
}
