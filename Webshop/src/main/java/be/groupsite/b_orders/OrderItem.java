package be.groupsite.b_orders;

public class OrderItem {
    private int itemID;
    private int orderID;
    private int productID;

    public OrderItem(int itemID, int orderID, int productID) {
        this.itemID = itemID;
        this.orderID = orderID;
        this.productID = productID;
    }
}
