package be.groupsite;
// Created without proper use of utility classes for input verification and with only basic exception handling.
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class WebApp {
    JdbcFacade jdbcFacade = new JdbcFacade();

// Dirty! But allowed really broad Exception required by call to jdbcFacade.close();
    public static void main(String[] args) throws Exception {
        WebApp webApp = new WebApp();
        webApp.startApp();

    }

    private void startApp() throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int userChoice;
        boolean running = true;
        do {
            System.out.println("You are logged in as: ___ adminLevel: ___.\n" +
                    "What would you like to do?\n" +
                    "1. Manage Stores\n" +
                    "2. Manage Users\n" +
                    "0. Quit");

            userChoice = Integer.parseInt(in.readLine());

            switch (userChoice) {
                case 1:
                   ManageStores manageStores = new ManageStores(jdbcFacade);
                   //todo this will be a user OBJ
                   manageStores.securityCheck(1);
                        break;
                case 2:
                    break;
                case 0:
                    running = false;
                    break;
                default:
                    System.out.println("userChoice switch error");
            }
        } while(running);

        jdbcFacade.close();
    }

}
