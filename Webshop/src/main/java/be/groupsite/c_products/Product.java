package be.groupsite.c_products;



import java.io.Serializable;

public class Product implements Serializable {
    private int internalProductId;
    private int storeCode;
    private String storePidSku;
    private double price;
    private String name;
    private String description;
    private boolean activeAndAvailable;

    public Product() {
    }

    public Product(int InternalProductId , int storeCode,
                   String storePidSku, double price, String name,
                   String description, boolean activeAndAvailable) {
        this.internalProductId = InternalProductId;
        this.storeCode = storeCode;
        this.storePidSku = storePidSku;
        this.price = price;
        this.name = name;
        this.description = description;
        this.activeAndAvailable = activeAndAvailable; //todo
    }

    public int getInternalProductId() {
        return internalProductId;
    }

    public int getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(int storeCode) {
        this.storeCode = storeCode;
    }

    public String getStorePidSku() {
        return storePidSku;
    }

    public void setStorePidSku(String storePidSku) {
        this.storePidSku = storePidSku;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActiveAndAvailable() {
        return activeAndAvailable;
    }

    public void setActiveAndAvailable(boolean activeAndAvailable) {
        this.activeAndAvailable = activeAndAvailable;
    }


    //All Strings non-null as in DB
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (internalProductId != product.internalProductId) return false;
        if (storeCode != product.storeCode) return false;
        if (Double.compare(product.price, price) != 0) return false;
        if (activeAndAvailable != product.activeAndAvailable) return false;
        if (!storePidSku.equals(product.storePidSku)) return false;
        if (!name.equals(product.name)) return false;
        return description.equals(product.description);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = internalProductId;
        result = 31 * result + storeCode;
        result = 31 * result + storePidSku.hashCode();
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + (activeAndAvailable ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format(
                "%s, [%d/%d/%s] costs %.2f euro %1s  Product active = %b",
                getName(),
                getInternalProductId(),
                getStoreCode(),
                getStorePidSku(),
                getPrice(),
                getDescription(),
                isActiveAndAvailable()
        );
    }
}
