package be.groupsite.c_products;

import java.util.List;

public interface ProductDao {

    //Initial
    Product getProductById(int id);
    void updateProduct(Product product);
    List<Product> getProductFuzzy(String fuzzy);
    List<Product> getProductAll();


    //As others added
    //List<Product> getProductByStore(Store store);
    //void addNewProduct(Product product, Store store);
}
