package be.groupsite;

import be.groupsite.d_store.Store;
import be.groupsite.d_store.daos.StandardStoreDao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ManageStores {
    Store storeToUpdate;
    Store updatedStore;
    Store retrievedStore;
    JdbcFacade jdbcFacade;
    StandardStoreDao standardStoreDao;
    boolean running = true;
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public ManageStores(JdbcFacade jdbcFacade) {
        this.jdbcFacade = jdbcFacade;
        this.standardStoreDao = new StandardStoreDao(jdbcFacade);
    }

    // Todo SRP manage stores should not be responsible for this.
    void securityCheck(int adminLevel) throws IOException {

       if (adminLevel == 1)
           manage();
       else
           System.out.println("You do not have authority to manage Stores");
   }

    private void manage() throws IOException {
       int userChoice = 0;
        do {
            System.out.print(
                    "What would you like to do?\n" +
                            "1. Create New Store\n" +
                            "2. Get Store by ID \n" +
                            "3. Update a store\n" +
                            "4. Remove a store\n" +
                            "5. Get current highest ID  \n" +
                            "0. Quit");

           userChoice = Integer.parseInt(in.readLine());

            switch (userChoice) {
                case 1:
                    Store newStore = createStoreObject();
                    System.out.println(newStore.toString() +
                            "\n was successfully created, and is set as inactive");
                    break;
                case 2:
                    this.retrievedStore = getStoreByID();
                    assert retrievedStore != null;
                    System.out.println("Here are your stores details:\n" +
                            retrievedStore.toString());
                    break;
                case 3:
                    this.updatedStore = updateStore();
                    System.out.println("Store Details have been updated to:\n" +
                            retrievedStore.toString());
                case 0:
                    in.close();
                    running = false;
                    break;
                default:
                    System.out.println("userChoice switch error");
            }

        }while (running);

    }

    private Store updateStore() throws IOException {
        String update;
        int updateType;
        boolean stillUpdating = true;
        System.out.println("Which store would you like to update?");
        storeToUpdate = getStoreByID();
        do {
            System.out.println("What would you like to update in the store above?" +
                    "1. StoreActive\n" +
                    "2. StoreStreet\n" +
                    "3. StorePostcode\n" +
                    "4. StoreTown\n" +
                    "0. Quit");

            updateType = Integer.parseInt(in.readLine());

            switch (updateType) {
                case 1:
            }

        }while(stillUpdating);
        return null;
    }

    private Store getStoreByID() throws IOException{
        int id = 0;
        System.out.println("Enter id of store to display details:");
        id = Integer.parseInt(in.readLine());
        return standardStoreDao.getStoreById(id) ;
    }

    private Store createStoreObject() throws IOException {
        Store newStore = null;
        String confirmation = null;
        boolean storeIncomplete = true;
        String storeName;
        String street;
        String postcode;
        String town;

        do {
            // todo would use normal input verification class
            System.out.println("Please enter new store name");
            storeName = in.readLine();
            System.out.println("and street");
            street = in.readLine();
            System.out.println("and postcode");
            postcode = in.readLine();
            System.out.println("and finally town");
            town = in.readLine();
            newStore = new Store(storeName, street, postcode, town);

            System.out.println("You are creating: \n" + newStore.toString() +
                    "\nCorrect? Yy to confirm any other to try again" );
            confirmation = in.readLine();

            if (confirmation.equals("Y") || confirmation.equals("y"))
               storeIncomplete = false;

        }while(storeIncomplete);

        return standardStoreDao.createStore(newStore);
    }
}
