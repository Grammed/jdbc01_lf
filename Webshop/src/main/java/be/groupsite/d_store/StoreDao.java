package be.groupsite.d_store;

import java.util.List;

public interface StoreDao {

    Store getStoreById(int id);
    Store createStore(Store storeObj);
    int getCurrentMax();
    Store updateStore(Store store, String column, String update);
    void removeStore(int Id);



    //Possible
    List<Store> getStoreFuzzy(String fuzzy);
    List<Store> getStoreAll();
}
