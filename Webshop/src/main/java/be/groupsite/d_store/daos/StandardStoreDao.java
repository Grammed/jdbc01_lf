package be.groupsite.d_store.daos;

import be.groupsite.JdbcFacade;
import be.groupsite.d_store.Store;
import be.groupsite.d_store.StoreDao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;

public class StandardStoreDao implements StoreDao {
    JdbcFacade jdbcFacade;

    public StandardStoreDao(){
        jdbcFacade = new JdbcFacade();
    }

    public StandardStoreDao(JdbcFacade jdbcFacade) {
        this.jdbcFacade = jdbcFacade;
    }

    public JdbcFacade getJdbcFacade() {
        return jdbcFacade;
    }

    public void setJdbcFacade(JdbcFacade jdbcFacade) {
        this.jdbcFacade = jdbcFacade;
    }

    @Override
    public Store getStoreById(int id) {

        String sql = ("select * from A_Store where StoreCode = ?;");
        Store store = new Store();
        try {
            PreparedStatement preparedStatement = jdbcFacade.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet results = preparedStatement.executeQuery();

            if (results.next()) {
                store.setStoreCode(results.getInt("StoreCode"));
                store.setStoreName(results.getString("StoreName"));
                store.setStoreActive(results.getBoolean("StoreActive"));
                store.setStoreStreet(results.getString("StoreStreet"));
                store.setStorePostcode(results.getString("StorePostcode"));
                store.setStoreTown(results.getString("StoreTown"));
                return store;
            }
        } catch (SQLException sqle) {
            System.out.println("@getStoreById(int id)");
            sqle.printStackTrace();
        }
        return store;
    }

    @Override
    public Store createStore(Store storeObj) {
        //todo check for duplicate store name, this will become secondary java side unique ID.
        //INSERT INTO `javadevt_Antw5`.`A_Store` (`StoreName`, `StoreActive`, `StoreStreet`, `StorePostcode`,
        //`StoreTown`) VALUES ('ActualStore03', '1', 'Store3Street', 'Store3Postcode', 'Store3Town');
        Store newStore;

        String sql = "Insert into A_Store (StoreName, StoreActive, StoreStreet, StorePostcode, StoreTown)" +
                "values (?, ?, ?, ?, ?);";
        PreparedStatement prep = jdbcFacade.prepareStatement(sql);

        try {
            prep.setString(1, storeObj.getStoreName());
            prep.setBoolean(2,storeObj.isStoreActive());
            prep.setString(3, storeObj.getStoreStreet());
            prep.setString(4, storeObj.getStorePostcode());
            prep.setString(5, storeObj.getStoreTown());
            prep.executeQuery();
            newStore = getStoreById(getCurrentMax());
            return newStore;

        }catch (SQLException sqle) {
            System.out.println("@createStore(Store storeObj)");
            sqle.printStackTrace();
        }

        return null;
    }

    @Override
    public int getCurrentMax() {
            try {
                ResultSet result = jdbcFacade.prepareStatement("select max(StoreCode) from A_Store;").executeQuery();

                if (result.next())
                    return result.getInt("MAX(StoreCode)");
                return 0;
            }catch (SQLException sqle) {
                System.out.println("@getCurrentMax()");
                sqle.printStackTrace();
            }
            return 0;
    }


    @Override
    public Store updateStore(Store store, String column, String update) {
        try {
            if(!(column.equals("StoreActive") ||column.equals("StoreStreet") ||
                    column.equals("StorePostcode") || column.equals("StoreTown")))
                throw new InputMismatchException("@updateStore(Store/String) string is not column heading");
        } catch (InputMismatchException ime) {
            ime.printStackTrace();
        }
        int storeCode = store.getStoreCode();
        int storeActiveToggle = 1;

        if(column.equals("StoreActive")) {
            if (store.isStoreActive())
                storeActiveToggle = 0;
        }

        String sql = "UPDATE A_Store SET `StoreActive` = ? WHERE `StoreCode` = ?;";
        PreparedStatement prep = jdbcFacade.prepareUpdatableStatement(sql);
        String sql1 = "UPDATE A_Store SET `StoreStreet` = ? WHERE `StoreCode` = ?;";
        PreparedStatement prep1 = jdbcFacade.prepareUpdatableStatement(sql1);
        String sql2 = "UPDATE A_Store SET `StorePostcode` = ? WHERE `StoreCode` = ?;";
        PreparedStatement prep2 = jdbcFacade.prepareUpdatableStatement(sql2);
        String sql3 = "UPDATE A_Store SET `StoreTown` = ? WHERE `StoreCode` = ?;";
        PreparedStatement prep3 = jdbcFacade.prepareUpdatableStatement(sql3);

        try {
            switch (column) {
                case "StoreActive":
                    prep.setInt(1, storeActiveToggle);
                    prep.setInt(2, storeCode);
                    prep.executeUpdate();
                    break;
                case "StoreStreet":
                    prep1.setString(1, update);
                    prep1.setInt(2, storeCode);
                    prep1.executeUpdate();
                    break;
                case "StorePostcode":
                    prep2.setString(1, update);
                    prep2.setInt(2, storeCode);
                    prep2.executeUpdate();
                    break;
                case "StoreTown":
                    prep3.setString(1, update);
                    prep3.setInt(2, storeCode);
                    prep3.executeUpdate();
                    break;
                default:
                    throw new InputMismatchException("@switch in updateStore(Store/String)");
            }
        }catch (SQLException sqle) {
            System.out.println("@updateStore(Store/String)");
            sqle.printStackTrace();
        }catch(InputMismatchException ime) {
            ime.printStackTrace();
        }
        return getStoreById(storeCode);
    }

    @Override
    public void removeStore(int Id) {
        String sql = "DELETE FROM A_Store WHERE `StoreCode` = ?;";

        try {
            PreparedStatement prep = jdbcFacade.prepareUpdatableStatement(sql);
            prep.setInt(1, Id);
            prep.executeQuery();

        } catch (SQLException sqle) {
            System.out.println("@removeStore(int)");
            sqle.printStackTrace();
        }




    }

    @Override
    public List<Store> getStoreFuzzy(String fuzzy) {
        return null;
    }

    @Override
    public List<Store> getStoreAll() {
        return null;
    }
}
