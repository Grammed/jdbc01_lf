package be.groupsite.d_store;

import java.io.Serializable;

public class Store implements Serializable {
   private int storeCode;
   private String storeName;
   private boolean storeActive;
   private String storeStreet;
   private String storePostcode;
   private String storeTown;



   public Store() {
   }

   public Store(int storeCode, String storeName, boolean storeActive,
                String storeStreet, String storePostcode, String storeTown) {
      this.storeCode = storeCode;
      this.storeName = storeName;
      this.storeActive = storeActive;
      this.storeStreet = storeStreet;
      this.storePostcode = storePostcode;
      this.storeTown = storeTown;
   }

   public Store(String storeName,
                String storeStreet, String storePostcode, String storeTown) {
      this.storeCode = 0; //Before added to database.
      this.storeName = storeName;
      this.storeActive = false;
      this.storeStreet = storeStreet;
      this.storePostcode = storePostcode;
      this.storeTown = storeTown;
   }

   public void setStoreCode(int storeCode) {
      this.storeCode = storeCode;
   }

   public int getStoreCode() {
      return storeCode;
   }

   public String getStoreName() {
      return storeName;
   }

   public void setStoreName(String storeName) {
      this.storeName = storeName;
   }

   public boolean isStoreActive() {
      return storeActive;
   }

   public void setStoreActive(boolean storeActive) {
      this.storeActive = storeActive;
   }

   public String getStoreStreet() {
      return storeStreet;
   }

   public void setStoreStreet(String storeStreet) {
      this.storeStreet = storeStreet;
   }

   public String getStorePostcode() {
      return storePostcode;
   }

   public void setStorePostcode(String storePostcode) {
      this.storePostcode = storePostcode;
   }

   public String getStoreTown() {
      return storeTown;
   }

   public void setStoreTown(String storeTown) {
      this.storeTown = storeTown;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Store store = (Store) o;

      if (storeCode != store.storeCode) return false;
      if (storeActive != store.storeActive) return false;
      if (!storeName.equals(store.storeName)) return false;
      if (!storeStreet.equals(store.storeStreet)) return false;
      if (!storePostcode.equals(store.storePostcode)) return false;
      return storeTown.equals(store.storeTown);
   }

   @Override
   public int hashCode() {
      int result = storeCode;
      result = 31 * result + storeName.hashCode();
      result = 31 * result + (storeActive ? 1 : 0);
      result = 31 * result + storeStreet.hashCode();
      result = 31 * result + storePostcode.hashCode();
      result = 31 * result + storeTown.hashCode();
      return result;
   }

   @Override
   public String toString() {
     return String.format(
              "Store %d - %s active=%b, Address: %s, -%s-, %s",
              getStoreCode(),
              getStoreName(),
              isStoreActive(),
              getStoreStreet(),
              getStorePostcode(),
              getStoreTown()
      );
   }
}
