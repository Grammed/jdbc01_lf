package be.groupsite.a_users;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserAdminLevelTest {
    UserAdminLevel userAdminLevel = new UserAdminLevel(UserType.CUSTOMER);

    @Test
    void testToString() {
        System.out.println(userAdminLevel.toString());
    }
}