package be.groupsite.a_users.daos;

import be.groupsite.JdbcFacade;
import be.groupsite.a_users.UserAdminLevel;
import be.groupsite.a_users.UserType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

// Have left these tests as they are originating from the SQL database, AND passed directly through to constructor,
// I would like to complete other classes and tests first, Ideally this would test against a latest permissions file?
// Then this would test how up to date the SQL table is, as well as if the constructor is working properly.
// Getting back into this took a really long time, OK lots of refactoring but a whole day for this and a test,
// though to be fair, I had to think about how I was going to handle the Users too, presently, the cost of making it
// up as you go along I suppose.

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StandardUserAdminLevelDaoTest {
    // CREATE DAO instance
    StandardUserAdminLevelDao standardUserAdminLevelDao = new StandardUserAdminLevelDao();
    // CREATE Blank instances
    UserAdminLevel customer = standardUserAdminLevelDao.createUserAdminLevel(UserType.CUSTOMER);
    UserAdminLevel storeOperative = standardUserAdminLevelDao.createUserAdminLevel(UserType.STORE_OPERATIVE);
    UserAdminLevel storeManager = standardUserAdminLevelDao.createUserAdminLevel(UserType.STORE_MANAGER);
    UserAdminLevel systemAdmin = standardUserAdminLevelDao.createUserAdminLevel(UserType.SYSTEM_ADMIN);

    @Test
    void createUserAdminLevel() {
        systemAdmin = standardUserAdminLevelDao.createUserAdminLevel(systemAdmin);
        standardUserAdminLevelDao.createUserAdminLevel(storeManager);
        standardUserAdminLevelDao.createUserAdminLevel(storeOperative);
        standardUserAdminLevelDao.createUserAdminLevel(customer);

        assertTrue(systemAdmin.isAddUsers());

    }

    @Test
    void testCreateUserAdminLevel() {
    }
}