package be.groupsite.d_store;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StoreTest {
    Store store = new Store(8, "Wibbles Curiosity Shoppe", false,
            "StreetSt", "PCodepd", "Towntwn");

    @Test
    void testToString() {
        System.out.println(store.toString());
    }
}