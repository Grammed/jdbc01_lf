package be.groupsite.d_store.daos;
import be.groupsite.d_store.Store;
import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


class StandardStoreDaoTest_02_RunSecond {
    StandardStoreDao standardStoreDao02;
    Store constructorMadeForComparison02;
    Store newCreatedStore02;

    @BeforeEach
            public void init() {
        standardStoreDao02 = new StandardStoreDao();
        constructorMadeForComparison02 = new Store(3, "ActualStore01", true,
                "Store1Street", "Store1Postcode", "Store1Town" );
        newCreatedStore02 = standardStoreDao02.getStoreById(standardStoreDao02.getCurrentMax());
    }


    @Order(6)
    @Test //STEP01a until StoreName made unique
    void testGetCurrentMax() {
        assertNotEquals(0, standardStoreDao02.getCurrentMax() );
    }

    @Order(7)
    @Test //STEP02a MANUAL : Tests update to store not created in test todo this is failing unless updated every time.
    void testManualUpdateStore() {
        String newStreet = "NewStreet110";
        String newPostcode = "NewPostcode101";
        String newTown = "NewTown10r";

        int currentMaxStoreID = standardStoreDao02.getCurrentMax();

        System.out.println("AndCurrentMaxIs " + standardStoreDao02.getCurrentMax());
        System.out.println("And new created store code is" + newCreatedStore02.getStoreCode());

        Store currentMaxStoreId = standardStoreDao02.getStoreById(currentMaxStoreID);
        boolean originalActive = currentMaxStoreId.isStoreActive();
        String originalStreet = currentMaxStoreId.getStoreStreet();
        System.out.println(originalStreet);
        String originalPostcode = currentMaxStoreId.getStorePostcode();
        System.out.println(originalPostcode);
        String originalTown = currentMaxStoreId.getStoreTown();
        System.out.println(originalTown);


        Store temp1 = standardStoreDao02.updateStore(currentMaxStoreId, "StoreActive", "toggle");
        assertNotEquals(originalActive, temp1.isStoreActive());
        Store temp2 = standardStoreDao02.updateStore(currentMaxStoreId, "StoreStreet", newStreet);
        assertNotEquals(originalStreet,temp2.getStoreStreet());
        Store temp3 = standardStoreDao02.updateStore(currentMaxStoreId, "StorePostcode", newPostcode);
        assertNotEquals(originalPostcode,temp3.getStorePostcode());
        Store temp4 = standardStoreDao02.updateStore(currentMaxStoreId, "StoreTown", newTown);
        assertNotEquals(originalTown,temp4.getStoreTown());

    }

    @IgnoreForBinding
    @Order(8)
    @Test //STEP03 (never used but included to self clean test)
    void removeStore() {

        standardStoreDao02.removeStore(standardStoreDao02.getCurrentMax());
    }
}