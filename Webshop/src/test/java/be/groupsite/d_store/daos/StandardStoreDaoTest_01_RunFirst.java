package be.groupsite.d_store.daos;


import be.groupsite.d_store.Store;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StandardStoreDaoTest_01_RunFirst {
    StandardStoreDao standardStoreDao;
    Store constructorMadeForComparison;
    Store newCreatedStore;

    @BeforeEach
            public void init() {
        standardStoreDao = new StandardStoreDao();
        constructorMadeForComparison = new Store(3, "ActualStore01", true,
                "Store1Street", "Store1Postcode", "Store1Town" );
        newCreatedStore  = standardStoreDao.getStoreById(standardStoreDao.getCurrentMax());
    }

    @Test
    @Order(1)
    void getStoreById() {
        System.out.println(constructorMadeForComparison.toString());
        Store returnedStore = standardStoreDao.getStoreById(3);
        System.out.println(returnedStore.toString());

        assertEquals(returnedStore, constructorMadeForComparison);
        assertEquals(returnedStore.hashCode(), constructorMadeForComparison.hashCode());
        assertNotSame(returnedStore, constructorMadeForComparison);
    }

    @Order(2)
    @Test //STEP01 Manual Store is unique, must be updated unless deleted in same test cycle.//NOT GOOD to have non independent tests.
    void testCreateStore() {
        System.out.println("From testCreateStore(): current Max Store ID == " + standardStoreDao.getCurrentMax());
        Store newCreatedStore = new Store("NewCreatedStore10", "asStreetNewCreated",
                "asPostcodeNewCreated", "asTownNewCreated");
        Store withSqlStoreCode = standardStoreDao.createStore(newCreatedStore);

        newCreatedStore.setStoreCode(withSqlStoreCode.getStoreCode());

        assertEquals(withSqlStoreCode, newCreatedStore );
        assertEquals(withSqlStoreCode.hashCode(), newCreatedStore .hashCode());
        assertNotSame(withSqlStoreCode, newCreatedStore );
        System.out.println("From testCreateStore(): Just created new store ID: " + standardStoreDao.getCurrentMax());
    }

    @Order(3)
    @Test //STEP01a until StoreName made unique
    void testGetCurrentMax() {
        assertNotEquals(0, standardStoreDao.getCurrentMax() );
    }

    @Order(4)
    @Test
    void getStoreFuzzy() {
    }

    @Order(5)
    @Test
    void getStoreAll() {
    }
}