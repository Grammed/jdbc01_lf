package be.groupsite.d_store.daos;

import be.groupsite.JdbcFacade;
import be.groupsite.d_store.Store;
import be.groupsite.d_store.daos.StandardStoreDao;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StoreDaoTestMockito {

    private StandardStoreDao standardStoreDao; // SUT

    private JdbcFacade jdbcFacade; //mock

    private Store store1; //mock
    private Store mostRecentStore; //mock

    @BeforeEach
    public void init() throws Exception {
        this.standardStoreDao = new StandardStoreDao();
        this.jdbcFacade = new JdbcFacade();
        this.store1 = new Store(3, "ActualStore01",
                true, "Store1Street",
                "Store1Postcode", "Store1Town" );
    }

    @Test
    public void testOne() {

    }


}
